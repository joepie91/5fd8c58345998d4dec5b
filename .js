> !!(false)
false
> !!(0)
false
> !!("")
false
> !!(null)
false
> !!(undefined)
false
> !!("0")
true
> !!("test")
true
> !!(-1)
true
> !!(true)
true
